#!/bin/bash
#
# Tag the current branch with the next
# version number, push the tag back to the remote.

set -e

# Tag and push
tag=$(semversioner current-version)
if [[ "${BITBUCKET_BRANCH}" =~ "test-" ]]; then
    tag="${tag}-test-${BITBUCKET_BUILD_NUMBER}"
fi

git tag -a -m "Tagging for release ${tag}" "${tag}"
git push origin ${tag}
