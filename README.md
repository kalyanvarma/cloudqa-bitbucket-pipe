# Bitbucket Pipelines Pipe: CloudQA

Triggers [CloudQA](https://cloudqa.io/) test suites via API.

You can generate an [API key](https://app.cloudqa.io/Account/UserProfile) and use this pipe to trigger tests in test suites.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: docker://almabase/cloudqa-bitbucket-pipe:0.1.3
  variables:
    CLOUDQA_TEST_SUITE_ID: '<string>'
    CLOUDQA_API_KEY: '<string>'
    # CLOUDQA_BROWSER: '<integer>' # Optional
    # RESULT_FETCH_INITIAL_DELAY: '<integer>' # Optional
    # RESULT_FETCH_INTERVAL: '<integer>' # Optional
    # RESULT_FETCH_TIMEOUT: '<integer>' # Optional
    # CLOUDQA_BASE_URL: '<string>' # Optional
    # CLOUDQA_RUN_SEQUENTIALLY: '<boolean>' # Optional
    # CLOUDQA_VARIABLES_JSON: '<json_string>' # Optional
    # CLOUDQA_ENV: '<string>' # Optional
    # CLOUDQA_TAG: '<string>' # Optional
    # SKIP_TESTS: '<boolean>' # Optional
    # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable           | Usage                                                       | Default           |
| --------------------- | ----------------------------------------------------------- | --------------------- |
| CLOUDQA_TEST_SUITE_ID (*) | Test suite identifier.  | `-` |
| CLOUDQA_API_KEY (*) | Secret API key used to make requests to CloudQA API. | `-` |
| CLOUDQA_BROWSER | Browser to run tests in (Chrome: 0, Firefox: 1). | `0` |
| RESULT_FETCH_INITIAL_DELAY | Seconds to wait before fetching test results. | `120` | 
| RESULT_FETCH_INTERVAL | Seconds to wait before subsequent result fetch. | `10` | 
| RESULT_FETCH_TIMEOUT | Seconds elapsed before we give up fetching results. | `3600` | 
| CLOUDQA_BASE_URL | Base URL for test suite (including the scheme). | `''` |
| CLOUDQA_RUN_SEQUENTIALLY  | `true` to run tests (in a test suite) sequentially. | `true` | 
| CLOUDQA_VARIABLES_JSON | Variables to be passed to test suite (in JSON format). | `'{}'` | 
| CLOUDQA_TAG | Test suite tag. | `''` |
| CLOUDQA_ENV | Test environment. | `''` |
| SKIP_TESTS | Skip execution of test cases. | `false` |
| DEBUG | Turn on extra debug information. | `false` |  

_(*) = required variable._

## Prerequisites

To run tests, you need to create a test suite in CloudQA. You can follow the instructions [here](https://doc.cloudqa.io/TestsuiteManagement.html) to create one. 
Also, generate an API Key [here](https://app.cloudqa.io/Account/UserProfile) to make requests to CloudQA API. 

## Examples

Basic example:

```yaml
- pipe: docker://almabase/cloudqa-bitbucket-pipe:0.1.3
  variables:
    CLOUDQA_TEST_SUITE_ID: '1234'
    CLOUDQA_API_KEY: 'aef123453e23eêe23e1ee32' 
    CLOUDQA_VARIABLES_JSON: '{"name": "almabase"}'
```

## Support

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce
