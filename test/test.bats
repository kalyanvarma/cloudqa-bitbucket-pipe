#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/cloudqa-bitbucket-pipe"}

  echo "Building image $DOCKER_IMAGE..."
  run docker build -t ${DOCKER_IMAGE} .
}

@test "1: All test cases pass successfully" {

    run docker run \
        -e CLOUDQA_VARIABLES_JSON="$CLOUDQA_VARIABLES_JSON" \
        -e CLOUDQA_API_KEY="$CLOUDQA_API_KEY" \
        -e CLOUDQA_TEST_SUITE_ID="$CLOUDQA_TEST_SUITE_ID" \
        -e RESULT_FETCH_INITIAL_DELAY=20 \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [[ "$output" =~ "All tests passed!" ]]
}

@test "2: Test suite execution times out" {

    run docker run \
        -e CLOUDQA_VARIABLES_JSON="$CLOUDQA_VARIABLES_JSON" \
        -e CLOUDQA_API_KEY="$CLOUDQA_API_KEY" \
        -e CLOUDQA_TEST_SUITE_ID="$CLOUDQA_TEST_SUITE_ID" \
        -e RESULT_FETCH_INITIAL_DELAY=2 \
        -e RESULT_FETCH_TIMEOUT=1 \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [[ "$output" =~ "Timed out waiting for test suite to finish execution" ]]
}

@test "3: Invalid Input(s)" {

    run docker run \
        -e CLOUDQA_VARIABLES_JSON=0 \
        -e CLOUDQA_API_KEY=1 \
        -e CLOUDQA_TEST_SUITE_ID='{"key": "value"}' \
        -e RESULT_FETCH_INITIAL_DELAY="a" \
        -e RESULT_FETCH_TIMEOUT="a" \
        -e CLOUDQA_BROWSER="a" \
        -e RESULT_FETCH_INTERVAL="a" \
        -e CLOUDQA_BASE_URL='{"key": "value"}' \
        -e CLOUDQA_TAG='{"key": "value"}' \
        -e CLOUDQA_ENV='{"key": "value"}' \
        -e DEBUG="a" \
        -e SKIP_TESTS='{"key": "value"}' \
        -e CLOUDQA_RUN_SEQUENTIALLY="a" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [[ "$output" =~ "Validation errors" ]]
    [[ "$output" =~ "CLOUDQA_VARIABLES_JSON" ]]
    [[ "$output" =~ "CLOUDQA_API_KEY" ]]
    [[ "$output" =~ "CLOUDQA_TEST_SUITE_ID" ]]
    [[ "$output" =~ "RESULT_FETCH_TIMEOUT" ]]
    [[ "$output" =~ "CLOUDQA_BROWSER" ]]
    [[ "$output" =~ "RESULT_FETCH_INTERVAL" ]]
    [[ "$output" =~ "CLOUDQA_BASE_URL" ]]
    [[ "$output" =~ "CLOUDQA_TAG" ]]
    [[ "$output" =~ "CLOUDQA_ENV" ]]
    [[ "$output" =~ "SKIP_TESTS" ]]
    [[ "$output" =~ "DEBUG" ]]
    [[ "$output" =~ "CLOUDQA_RUN_SEQUENTIALLY" ]]
}
