import time
import json
import requests

from bitbucket_pipes_toolkit import Pipe


validation_schema = {
    # mandatory
    'CLOUDQA_TEST_SUITE_ID': {'type': 'integer', 'required': True},
    'CLOUDQA_API_KEY': {'type': 'string', 'required': True},

    # optional
    'CLOUDQA_BROWSER': {'type': 'integer', 'required': False, 'default': 0},
    'RESULT_FETCH_INITIAL_DELAY': {'type': 'integer', 'required': False, 'default': 120},
    'RESULT_FETCH_INTERVAL': {'type': 'integer', 'required': False, 'default': 10},
    'RESULT_FETCH_TIMEOUT': {'type': 'integer', 'required': False, 'default': 3600},
    'CLOUDQA_BASE_URL': {'type': 'string', 'required': False, 'default': ''},
    'CLOUDQA_RUN_SEQUENTIALLY': {'type': 'boolean', 'required': False, 'default': True},
    'CLOUDQA_VARIABLES_JSON': {'type': 'dict', 'required': False, 'default': {}},
    'CLOUDQA_TAG': {'type': 'string', 'required': False, 'default': ''},
    'CLOUDQA_ENV': {'type': 'string', 'required': False, 'default': ''},
    'SKIP_TESTS': {'type': 'boolean', 'required': False, 'default': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
}


class CloudQAClient(object):
    BASE_URL = 'https://app.cloudqa.io'
    RUNS_POST_ENDPOINT = '/api/v1/suites/{test_suite_id}/runs'
    RUN_STATUS_GET_ENDPOINT = '/api/v1/suites/{test_suite_id}/runs/{run_id}'

    def __init__(self, api_key):
        self.api_key = api_key
        self.request_headers = {
            'Content-Type': 'application/json',
            'Authorization': 'ApiKey {api_key}'.format(api_key=self.api_key),
        }

    def get_response(self, url, request_method='GET', request_payload=None):
        """Send HTTP request and fetch response from CloudQA API."""
        return requests.request(method=request_method, url=url, headers=self.request_headers, data=request_payload)


class CloudQATestSuite(object):
    def __init__(self, options):
        self.id = str(options['CLOUDQA_TEST_SUITE_ID'])
        self.browser = options['CLOUDQA_BROWSER']
        self.base_url = options['CLOUDQA_BASE_URL']
        self.variables = options['CLOUDQA_VARIABLES_JSON']
        self.tag = options['CLOUDQA_TAG']
        self.env = options['CLOUDQA_ENV']
        self.is_sequential = options['CLOUDQA_RUN_SEQUENTIALLY']
        self.result_fetch_initial_delay = options['RESULT_FETCH_INITIAL_DELAY']
        self.result_fetch_interval = options['RESULT_FETCH_INTERVAL']
        self.result_fetch_timeout = options['RESULT_FETCH_TIMEOUT']
        self.api_client = CloudQAClient(options['CLOUDQA_API_KEY'])

    def trigger_tests(self):
        """Trigger tests in CloudQA via API."""
        request_payload = {
            'Browser': self.browser,
            'BaseUrl': self.base_url,
            'Variables': self.variables,
            'EnvironmentName': self.env,
            'Tag': self.tag,
            'RunSequentially': self.is_sequential,
        }

        url = self.api_client.BASE_URL + self.api_client.RUNS_POST_ENDPOINT.format(test_suite_id=self.id)
        response = self.api_client.get_response(url, request_method='POST', request_payload=json.dumps(request_payload))
        response.raise_for_status()

        return response.json()['runId']

    def did_tests_pass(self, run_id):
        """
        Check whether the tests (pertaining to a run) passed.
        Sleep for a bit (let the tests run) and start polling the test results.
        """
        remaining_time = self.result_fetch_timeout

        time.sleep(self.result_fetch_initial_delay)
        remaining_time -= self.result_fetch_initial_delay

        run_status_url = self.api_client.BASE_URL + self.api_client.RUN_STATUS_GET_ENDPOINT.format(
            test_suite_id=self.id, run_id=run_id
        )
        while remaining_time > 0:
            api_response = self.api_client.get_response(run_status_url).json()
            if api_response['status'] == 'Completed':
                if api_response['result'] == 'Passed':
                    return True

                return False

            time.sleep(self.result_fetch_interval)
            remaining_time -= self.result_fetch_interval

        raise Exception('Timed out waiting for test suite to finish execution')


class CloudQAPipe(Pipe):
    def run(self):
        """
        Run the pipe.

        The main entry point for pipe execution. This will trigger the tests and wait
        for the tests to finish (and timeout if they do not finish within the specified time interval)
        """
        super().run()

        if self.variables['SKIP_TESTS']:
            self.success('Skipping test suite execution...', do_exit=True)

        test_suite = CloudQATestSuite(self.variables)

        try:
            run_id = test_suite.trigger_tests()
            self.log_debug('Successfully triggered tests, Run ID: {run_id}'.format(run_id=run_id))
        except requests.exceptions.HTTPError as error:
            self.fail('Failed to execute test suite (request to API failed) (Error: {})'.format(str(error)))

        try:
            tests_passed = test_suite.did_tests_pass(run_id)
        except Exception as exc:
            self.fail('An Exception was raised (while checking the test result): {}'.format(str(exc)))

        if tests_passed:
            self.success('All tests passed!', do_exit=True)

        self.fail('One or more tests failed!')


if __name__ == '__main__':
    pipe = CloudQAPipe(pipe_metadata_file='/pipe.yml', schema=validation_schema)
    pipe.run()
