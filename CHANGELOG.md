# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.3

- patch: Added option to skip test suite execution.

## 0.1.2

- patch: Added CLOUDQA_ENV, corrected README.md

## 0.1.1

- patch: Fixed default value for CLOUDQA_VARIABLES_JSON.

## 0.1.0

- minor: Added CloudQA pipe.

